<?php

require_once __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use GuzzleHttp\Client as Guzzle;

$connection = new AMQPStreamConnection('rabbitmq', 5672, 'guest', 'guest');
$channel = $connection->channel();
$channel->queue_declare('hello', false, false, false, false);
$client = new Guzzle([
    'base_uri' => 'http://nginx',
]);

echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";

$callback = function ($msg) use ($client) {
  $message = " [x] Received {$msg->body}\n";
  $response = $client->post('/handle.php', [
    'form_params' => [
      'message' => $message
    ],
    'body' => $message
  ]);
  $body = $response->getBody();
  echo $body->getContents();
  // echo " [x] Received ", $msg->body, "\n";
};

$channel->basic_consume('hello', '', false, true, false, false, $callback);

while(count($channel->callbacks)) {
  $channel->wait();
}
