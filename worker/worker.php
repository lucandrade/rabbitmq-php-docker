<?php

require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use GuzzleHttp\Client as Guzzle;

$connection = new AMQPStreamConnection('rabbitmq', 5672, 'guest', 'guest');
$channel = $connection->channel();
$channel->queue_declare('task_queue', false, true, false, false);
$client = new Guzzle([
    'base_uri' => 'http://nginx',
]);

echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";

$callback = function ($msg) use ($client) {
  $message = " [x] Received {$msg->body}\n";
  $response = $client->post('/handle.php', [
    'form_params' => [
      'message' => $message
    ],
    'body' => $message
  ]);
  $body = $response->getBody();
  echo $body->getContents();
  sleep(substr_count($msg->body, '.'));
  echo " [x] Done", "\n";
  $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
};

$channel->basic_qos(null, 2, null);
$channel->basic_consume('task_queue', '', false, false, false, false, $callback);

while(count($channel->callbacks)) {
  $channel->wait();
}

$channel->close();
$connection->close();
